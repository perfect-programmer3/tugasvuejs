<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
// use App\Movie;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class MovieController extends Controller
{
 /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table movies
        $movies = Movie::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Movie Review List',
            'data'    => $movies
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find movie review by ID
        $movie = Movie::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Review Detail',
            'data'    => $movie
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'sinopsis' => 'required',
            'title'   => 'required',
            'review' => 'required',
            'rating' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $movie = Movie::create([
            'sinopsis'     => $request->sinopsis,
            'title'     => $request->title,
            'review'   => $request->review,
            'rating'   => $request->rating
        ]);

        //success save to database
        if($movie) {

            return response()->json([
                'success' => true,
                'message' => 'Movie Created',
                'data'    => $movie  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Movie Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $movie
     * @return void
     */
    public function update(Request $request, Movie $movie)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'sinopsis' => 'required',
            'title'   => 'required',
            'review' => 'required',
            'rating' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find movie review by ID
        $movie = Movie::findOrFail($movie->id);

        if($movie) {

            //update review
            $movie->update([
                'sinopsis'  =>$request->sinopsis, 
                'title'     => $request->title,
                'review'   => $request->review,
                'rating'   => $request->rating
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Review Updated',
                'data'    => $movie
            ], 200);

        }

        //data review not found
        return response()->json([
            'success' => false,
            'message' => 'Review Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find review by ID
        $movie = Movie::findOrfail($id);

        if($movie) {

            //delete review
            $movie->delete();

            return response()->json([
                'success' => true,
                'message' => 'Review Deleted',
            ], 200);

        }

        //data review not found
        return response()->json([
            'success' => false,
            'message' => 'Review Not Found',
        ], 404);
    }
}
